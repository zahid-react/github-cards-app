import React from 'react';
import './App.css';
import  CardList from './components/cardlist';
import  Form from './components/form';
import  testData from './data/data';


class App extends React.Component {
	state = {
		profiles:[]
	}
	addNewProfile = (profileData) => {
		this.setState(prevState => ({
			profiles: [...prevState.profiles, profileData]
		}))
	}
	render() {
  	return (
    	<div className="container">
    	  	<div className="header">{this.props.title}</div>
			<Form onSubmit={this.addNewProfile}/>
        	<CardList profiles={this.state.profiles}/>
    	</div>
    );
  }	
}

export default App;
